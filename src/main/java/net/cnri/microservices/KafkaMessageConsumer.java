package net.cnri.microservices;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.regex.Pattern;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.errors.WakeupException;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KafkaMessageConsumer implements MessageConsumer {
    private static Logger logger = LoggerFactory.getLogger(KafkaMessageConsumer.class);

    private final ExecutorService exec;
    private final KafkaConsumer<String,String> consumer;
    private volatile boolean subscribed = false;
    private Consumer<String> callback;
    private String currentTopic;
    private String groupId;

    public KafkaMessageConsumer(String bootstrapServers, String groupId) {
        this(bootstrapServers, groupId, null);
    }

    public KafkaMessageConsumer(String bootstrapServers, String groupId, Map<String, String> consumerConfig) {
        this.exec = Executors.newSingleThreadExecutor();
        this.consumer = constructConsumer(bootstrapServers, groupId, consumerConfig);
        this.groupId = groupId;
    }

    private static KafkaConsumer<String,String> constructConsumer(String bootstrapServers, String groupId, Map<String, String> consumerConfig) {
        Properties props = new Properties();
        if (consumerConfig != null) props.putAll(consumerConfig);
        props.putIfAbsent("bootstrap.servers", bootstrapServers);
        if (groupId != null) props.putIfAbsent("group.id", groupId);
        props.putIfAbsent("key.deserializer", StringDeserializer.class.getName());
        props.putIfAbsent("value.deserializer", StringDeserializer.class.getName());
        props.putIfAbsent("enable.auto.commit", "false");
        props.putIfAbsent("auto.offset.reset", "earliest");
        props.putIfAbsent("metadata.max.age.ms", 5000);
        props.putIfAbsent("client.id", "consumer-" + UUID.randomUUID());
        KafkaConsumer<String,String> consumer = new KafkaConsumer<>(props);
        return consumer;
    }

    @Override
    public void start(String topic, Consumer<String> callbackParam) {
        if (subscribed) throw new IllegalStateException();
        logger.info("Subscribing to " + topic);
        this.callback = callbackParam;
        submitAndWait(() -> consumer.subscribe(Arrays.asList(topic)));
        subscribed = true;
        currentTopic = topic;
        exec.execute(this::runAndLogErrors);
    }

    public void stop() {
        subscribed = false;
        submitAndWait(consumer::unsubscribe);
    }

    private void submitAndWait(Runnable r) {
        try {
            exec.submit(r).get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof RuntimeException) throw (RuntimeException)e.getCause();
            throw new RuntimeException(e.getCause());
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    @Override
    public String getTopic() {
        return currentTopic;
    }

    private void runAndLogErrors() {
        try {
            run();
        } catch (WakeupException e) {
            // ignore
        } catch (Throwable e) {
            logger.error("Fatal error in KafkaMessageConsumer", e);
        }
    }

    private void run() {
        logger.info("Consumer " + groupId + " starts, topic " + currentTopic);
        while (subscribed) {
            ConsumerRecords<String, String> records = consumer.poll(Duration.of(100, ChronoUnit.MILLIS));
            for (TopicPartition topicPartition : records.partitions()) {
                for (ConsumerRecord<String, String> record : records.records(topicPartition)) {
                    if (!subscribed) return;
                    String msg = record.value();
//                    logger.info("Consume (" + groupId + "): " + msg);
                    callback.accept(msg);
                }
                consumer.commitSync();
            }
        }
    }

    public void readAndDiscardAllMessages() {
        consumer.subscribe(Pattern.compile(".*"));
        try {
            while (true) {
                ConsumerRecords<String, String> records = consumer.poll(Duration.of(0, ChronoUnit.MILLIS));
                if (records.isEmpty()) return;
                consumer.commitSync();
            }
        } finally {
            consumer.unsubscribe();
        }
    }

    @Override
    public synchronized void shutdown() {
        subscribed = false;
        consumer.wakeup();
        exec.execute(consumer::close);
        exec.shutdown();
        try {
            exec.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
}
