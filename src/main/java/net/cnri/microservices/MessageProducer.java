package net.cnri.microservices;

import java.util.Collection;
import java.util.Collections;

public interface MessageProducer {

    default void produce(String topic, String key, String message) {
        produce(new Message(topic, key, message));
    }
    default void produce(Message message) {
        produce(Collections.singletonList(message));
    }
    void produce(Collection<Message> messages);
    void shutdown();
}
