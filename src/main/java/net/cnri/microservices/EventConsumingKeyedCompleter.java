package net.cnri.microservices;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;

public abstract class EventConsumingKeyedCompleter<EventT, ResultT> implements KeyedCompleter<ResultT> {

    private final Collection<MultithreadedKafkaConsumer> messageConsumers;
    private final ConcurrentMap<String, CompletableFuture<ResultT>> waiters = new ConcurrentHashMap<>();

    public EventConsumingKeyedCompleter(int numConsumers, Collection<String> topics, String groupId, Map<String, String> consumerConfig, String kafkaBootstrapServers, Alerter alerter, ExecutorService taskRunner) {
        this.messageConsumers = new ArrayList<>();
        for (int i = 0; i < numConsumers; i++) {
            MultithreadedKafkaConsumer messageConsumer = new MultithreadedKafkaConsumer(topics,
                groupId, consumerConfig, kafkaBootstrapServers, alerter, taskRunner);
            this.messageConsumers.add(messageConsumer);
        }
    }

    @Override
    public void init() {
        for (MultithreadedKafkaConsumer messageConsumer : messageConsumers) {
            messageConsumer.start(this::consume);
        }
    }

    @Override
    public void shutdown() {
        for (MultithreadedKafkaConsumer messageConsumer : messageConsumers) {
            messageConsumer.shutdown();
        }
    }

    private void consume(String message) {
        EventT event = deserialize(message);
        if (!filter(event)) {
            return;
        }
        String key = getKey(event);
        CompletableFuture<ResultT> future = waiters.remove(key);
        if (future == null) {
            return;
        }
        ResultT result = getResult(event);
        future.complete(result);
    }

    abstract protected EventT deserialize(String message);

    @SuppressWarnings("unused")
    protected boolean filter(EventT event) {
        return true;
    }

    abstract protected String getKey(EventT event);

    abstract protected ResultT getResult(EventT event);

    @Override
    public CompletableFuture<ResultT> register(String key) {
        ensureStarted();
        CompletableFuture<ResultT> future = new CompletableFuture<>();
        if (waiters.putIfAbsent(key, future) != null) {
            throw new IllegalStateException("Already waiting on " + key);
        }
        return future;
    }

    private void ensureStarted() {
        for (MultithreadedKafkaConsumer messageConsumer : messageConsumers) {
            messageConsumer.ensureStarted();
        }
    }

    @Override
    public void clear(String key) {
        waiters.remove(key);
    }
}
