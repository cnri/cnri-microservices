package net.cnri.microservices;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.regex.Pattern;

import org.apache.kafka.clients.consumer.ConsumerRebalanceListener;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.errors.WakeupException;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MultithreadedKafkaConsumer {
    private static final Logger logger = LoggerFactory.getLogger(MultithreadedKafkaConsumer.class);

    private static final int MAX_LAG = 100_000;

    private final ExecutorService exec;
    private final KafkaConsumer<String, String> consumer;
    private final ConcurrentMap<TopicPartition, HandledOffsetsTracker> offsets = new ConcurrentHashMap<>();
    private final ConcurrentMap<TopicPartition, OffsetAndMetadata> nextToCommit = new ConcurrentHashMap<>();
    private final Alerter alerter;
    private final ConcurrentCountingHashMap<TopicPartition> messagesBeingProcessed = new ConcurrentCountingHashMap<>();
    private final ConcurrentMap<TopicPartition, CountDownLatch> topicsBeingRevoked = new ConcurrentHashMap<>();
    private final RebalanceListener rebalanceListener;
    private final ExecutorService taskRunner;
    private final StripedExecutorService stripedTaskRunner;
    private final CountDownLatch startLatch = new CountDownLatch(1);

    private volatile boolean running = false;
    private boolean shutdown = false;
    private BiConsumer<String, Long> callback;
    private Function<String, Object> stripePicker;

    public MultithreadedKafkaConsumer(Pattern pattern, String groupId,
        Map<String, String> consumerConfig, String kafkaBootstrapServers, Alerter alerter, ExecutorService taskRunner) {
        this(pattern, null, groupId, consumerConfig, kafkaBootstrapServers, alerter, taskRunner, null);
    }

    public MultithreadedKafkaConsumer(Collection<String> topics, String groupId,
        Map<String, String> consumerConfig, String kafkaBootstrapServers, Alerter alerter, ExecutorService taskRunner) {
        this(null, topics, groupId, consumerConfig, kafkaBootstrapServers, alerter, taskRunner, null);
    }

    public MultithreadedKafkaConsumer(Pattern pattern, String groupId,
        Map<String, String> consumerConfig, String kafkaBootstrapServers, Alerter alerter, StripedExecutorService stripedTaskRunner) {
        this(pattern, null, groupId, consumerConfig, kafkaBootstrapServers, alerter, null, stripedTaskRunner);
    }

    public MultithreadedKafkaConsumer(Collection<String> topics, String groupId,
        Map<String, String> consumerConfig, String kafkaBootstrapServers, Alerter alerter, StripedExecutorService stripedTaskRunner) {
        this(null, topics, groupId, consumerConfig, kafkaBootstrapServers, alerter, null, stripedTaskRunner);
    }

    private MultithreadedKafkaConsumer(Pattern pattern, Collection<String> topics, String groupId,
        Map<String, String> consumerConfig, String kafkaBootstrapServers, Alerter alerter,
        ExecutorService taskRunner, StripedExecutorService stripedTaskRunner) {
        Properties props = new Properties();
        if (consumerConfig != null) props.putAll(consumerConfig);
        props.putIfAbsent("bootstrap.servers", kafkaBootstrapServers);
        props.putIfAbsent("group.id", groupId);
        props.putIfAbsent("key.deserializer", StringDeserializer.class.getName());
        props.putIfAbsent("value.deserializer", StringDeserializer.class.getName());
        props.putIfAbsent("enable.auto.commit", "false");
        props.putIfAbsent("auto.offset.reset", "earliest");
        props.putIfAbsent("metadata.max.age.ms", 5000);
        props.putIfAbsent("client.id", "consumer-" + UUID.randomUUID());
        consumer = new KafkaConsumer<>(props);
        this.rebalanceListener = new RebalanceListener();
        if (pattern != null) {
            consumer.subscribe(pattern, rebalanceListener);
        } else {
            consumer.subscribe(topics, rebalanceListener);
        }
        exec = Executors.newSingleThreadExecutor();
        if (alerter == null) {
            this.alerter = new NoOpAlerter();
        } else {
            this.alerter = alerter;
        }
        this.taskRunner = taskRunner;
        this.stripedTaskRunner = stripedTaskRunner;
    }

    @SuppressWarnings("hiding")
    public synchronized void start(Consumer<String> callback) {
        this.start((message, timestamp) -> callback.accept(message));
    }

    @SuppressWarnings("hiding")
    public synchronized void start(BiConsumer<String, Long> callback) {
        if (running) throw new IllegalStateException("already started");
        if (shutdown) throw new IllegalStateException("already shut down");
        if (taskRunner == null) throw new IllegalStateException("striping required");
        this.callback = callback;
        completeStart();
    }

    @SuppressWarnings("hiding")
    public synchronized void start(Consumer<String> callback, Function<String, Object> stripePicker) {
        this.start((message, timestamp) -> callback.accept(message), stripePicker);
    }

    @SuppressWarnings("hiding")
    public synchronized void start(BiConsumer<String, Long> callback, Function<String, Object> stripePicker) {
        if (running) throw new IllegalStateException("already started");
        if (shutdown) throw new IllegalStateException("already shut down");
        if (stripedTaskRunner == null) throw new IllegalStateException("striping not supported");
        this.callback = callback;
        this.stripePicker = stripePicker;
        completeStart();
    }

    private void completeStart() {
        running = true;
        exec.execute(this::runAndLogErrors);
    }

    public void ensureStarted() {
        try {
            startLatch.await(Long.MAX_VALUE, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    private void runAndLogErrors() {
        try {
            run();
        } catch (WakeupException e) {
            // ignore
        } catch (Throwable e) {
            logger.error("Fatal error in MultithreadedKafkaConsumer", e);
            alerter.alert("Fatal error in MultithreadedKafkaConsumer: " + e);
        }
    }

    private void run() {
        boolean firstTime = true;
        long backoff = 50;
        while (running) {
            Duration pollDuration = Duration.of(firstTime ? 0 : 100, ChronoUnit.MILLIS);
            ConsumerRecords<String, String> records = consumer.poll(pollDuration);
            if (firstTime) {
                startLatch.countDown();
                firstTime = false;
            }
            boolean isError = false;
            for (TopicPartition topicPartition : records.partitions()) {
                if (!running) break;
                for (ConsumerRecord<String, String> record : records.records(topicPartition)) {
                    if (!running) break;
                    if (isError) {
                        consumer.seek(topicPartition, record.offset());
                        break;
                    }
                    try {
                        KafkaCommittableTask task = new KafkaCommittableTask(record);
                        if (taskRunner != null) {
                            taskRunner.submit(() -> runTask(task));
                        } else if (stripedTaskRunner != null) {
                            Object stripe = pickStripe(task);
                            stripedTaskRunner.submit(stripe, () -> runTask(task));
                        }
                        backoff = 50;
                    } catch (RejectedExecutionException e) {
                        isError = true;
                        consumer.seek(topicPartition, record.offset());
                        break;
                    }
                }
            }
            if (isError) {
                backoff = sleepAndIncreaseBackoff(backoff);
            }
            sendCommitsToKafka();
        }
        awaitLockedTasks();
        sendCommitsToKafka();
    }

    private void runTask(KafkaCommittableTask task) {
        try {
            if (task.isCancelled()) return;
            task.lock();
            try {
                if (task.isCancelled()) return;
                callback.accept(task.getMessage(), task.getTimestamp());
                task.commit();
            } finally {
                task.release();
            }
        } catch (Throwable e) {
            logger.error("Unexpected error in MultithreadedKafkaConsumer processing", e);
            alerter.alert("Unexpected error in MultithreadedKafkaConsumer processing " + e);
        }
    }

    private Object pickStripe(KafkaCommittableTask task) {
        Object stripe;
        if (stripePicker == null) {
            stripe = new Object();
        } else {
            stripe = stripePicker.apply(task.getMessage());
        }
        return stripe;
    }

    private long sleepAndIncreaseBackoff(long backoff) {
        try {
            Thread.sleep(backoff);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        backoff *= 2;
        if (backoff > 120_000) {
            logger.error("Consumer queue at maximum backoff");
            alerter.alert("Consumer queue at maximum backoff");
            backoff = 120_000;
        }
        return backoff;
    }

    private void sendCommitsToKafka() {
        if (!nextToCommit.isEmpty()) {
            Map<TopicPartition, OffsetAndMetadata> commits = new HashMap<>();
            for (Map.Entry<TopicPartition, OffsetAndMetadata> entry : nextToCommit.entrySet()) {
                nextToCommit.remove(entry.getKey(), entry.getValue());
                commits.put(entry.getKey(), entry.getValue());
            }
            if (!commits.isEmpty()) consumer.commitSync(commits);
        }
    }

    private void awaitLockedTasks() {
        Set<TopicPartition> topicPartitions = messagesBeingProcessed.keySet();
        for (TopicPartition topicPartition : topicPartitions) {
            topicsBeingRevoked.put(topicPartition, new CountDownLatch(1));
        }
        for (TopicPartition topicPartition : topicPartitions) {
            if (messagesBeingProcessed.get(topicPartition) > 0) {
                try { topicsBeingRevoked.get(topicPartition).await(); } catch (InterruptedException e) { Thread.currentThread().interrupt(); }
            }
        }
    }

    class KafkaCommittableTask {
        private final ConsumerRecord<String, String> record;
        private final TopicPartition topicPartition;

        public KafkaCommittableTask(ConsumerRecord<String, String> record) {
            this.record = record;
            this.topicPartition = new TopicPartition(record.topic(), record.partition());
        }

        public String getMessage() {
            return record.value();
        }

        public long getTimestamp() {
            return record.timestamp();
        }

        public void lock() {
            messagesBeingProcessed.incrementAndGet(topicPartition);
        }

        public void release() {
            if (messagesBeingProcessed.decrementAndGet(topicPartition) == 0) {
                CountDownLatch latch = topicsBeingRevoked.get(topicPartition);
                if (latch != null) latch.countDown();
            }
        }

        public boolean isCancelled() {
            if (!running) return true;
            if (topicsBeingRevoked.containsKey(topicPartition)) return true;
            if (!offsets.containsKey(topicPartition)) return true;
            return false;
        }

        public void commit() {
            HandledOffsetsTracker tracker = offsets.get(topicPartition);
            if (tracker == null) {
                logger.error("Message in revoked partition " + topicPartition + "!  Offset: " + record.offset() + " Message: " + record.value());
                alerter.alert("Message in revoked partition " + topicPartition + "!  Offset: " + record.offset() + " Message: " + record.value());
                return;
            }
            long handledUpTo = tracker.handle(record.offset());
            if (handledUpTo >= record.offset()) {
                nextToCommit.put(topicPartition, new OffsetAndMetadata(handledUpTo + 1));
            } else if (record.offset() > handledUpTo + MAX_LAG) {
                handledUpTo = tracker.skipTo(handledUpTo + MAX_LAG/2);
                nextToCommit.put(topicPartition, new OffsetAndMetadata(handledUpTo + 1));
            }
        }
    }

    class RebalanceListener implements ConsumerRebalanceListener {

        @Override
        public synchronized void onPartitionsAssigned(Collection<TopicPartition> topicPartitions) {
            for (TopicPartition topicPartition : topicPartitions) {
                long offset = consumer.position(topicPartition);
                offsets.putIfAbsent(topicPartition, new HandledOffsetsTracker(offset - 1, topicPartition.toString(), alerter));
            }
        }

        @Override
        public synchronized void onPartitionsRevoked(Collection<TopicPartition> topicPartitions) {
            for (TopicPartition topicPartition : topicPartitions) {
                topicsBeingRevoked.put(topicPartition, new CountDownLatch(1));
            }
            for (TopicPartition topicPartition : topicPartitions) {
                if (messagesBeingProcessed.get(topicPartition) == 0) {
                    offsets.remove(topicPartition);
                    topicsBeingRevoked.remove(topicPartition);
                } else {
                    try { topicsBeingRevoked.get(topicPartition).await(); } catch (InterruptedException e) { Thread.currentThread().interrupt(); }
                    offsets.remove(topicPartition);
                    topicsBeingRevoked.remove(topicPartition);
                }
            }
        }
    }

    public synchronized void shutdown() {
        if (running) {
            shutdown = true;
            running = false;
            consumer.wakeup();
            exec.execute(consumer::close);
            exec.shutdown();
            try {
                exec.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }
}
