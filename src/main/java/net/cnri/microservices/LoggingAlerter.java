package net.cnri.microservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggingAlerter implements Alerter {
    private static final Logger logger = LoggerFactory.getLogger(LoggingAlerter.class);

    @Override
    public void alert(String s) {
        logger.error(s);
    }

}
