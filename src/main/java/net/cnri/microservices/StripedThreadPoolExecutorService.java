package net.cnri.microservices;

import java.lang.Thread.UncaughtExceptionHandler;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class StripedThreadPoolExecutorService implements StripedExecutorService {
    private final ExecutorService execServ;
    private final List<BlockingQueue<Runnable>> tasksByStripe;
    private final List<AtomicBoolean> isStripeActive;
    private final int stripes;
    private final UncaughtExceptionHandler ueh;
    private final AtomicInteger numTasks = new AtomicInteger();
    private final int maxTasks;
    
    public StripedThreadPoolExecutorService(int stripes, int threads, int maxTasks, UncaughtExceptionHandler ueh) {
        this.stripes = stripes;
        this.maxTasks = maxTasks;
        this.ueh = ueh;
        this.execServ = Executors.newFixedThreadPool(threads);
        this.tasksByStripe = new ArrayList<>(stripes);
        this.isStripeActive = new ArrayList<>(stripes);
        for (int i = 0; i < stripes; i++) {
            this.tasksByStripe.add(new LinkedBlockingQueue<>());
            this.isStripeActive.add(new AtomicBoolean());
        }
    }
    
    @Override
    public void submit(Object stripeObject, Runnable task) throws RejectedExecutionException {
        if (numTasks.incrementAndGet() > maxTasks) {
            numTasks.decrementAndGet();
            throw new RejectedExecutionException();
        }
        int hashCode = stripeObject == null ? 0 : stripeObject.hashCode();
        int stripe = (smear(hashCode) % stripes + stripes) % stripes;
        tasksByStripe.get(stripe).add(task);
        if (!isStripeActive.get(stripe).getAndSet(true)) {
            execServ.submit(() -> runStripeAndLogError(stripe));
        }
    }

    private void runStripeAndLogError(int stripe) {
        try {
            runStripe(stripe);
        } catch (Throwable e) {
            ueh.uncaughtException(Thread.currentThread(), e);
        }
    }
    
    private void runStripe(int stripe) {
        BlockingQueue<Runnable> queue = tasksByStripe.get(stripe);
        AtomicBoolean isActive = isStripeActive.get(stripe); // always active on start
        Runnable task;
        while (true) {
            // run all the tasks for this stripe
            while ((task = queue.poll()) != null) {
                numTasks.decrementAndGet();
                try {
                    task.run();
                } catch (Exception e) {
                    ueh.uncaughtException(Thread.currentThread(), e);
                }
            }
            // ran out of tasks; say that we are done
            isActive.set(false);
            // if still no more tasks, exit
            if (queue.isEmpty()) return;
            // a task snuck in.  If another thread is handling it (because isActive was set to true), exit.  Otherwise keep going
            if (isActive.getAndSet(true)) return;
        }
    }
    
    @Override
    public void shutdown() {
        execServ.shutdown();
        try {
            execServ.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
    
    /*
     * This method was written by Doug Lea with assistance from members of JCP JSR-166 Expert Group
     * and released to the public domain, as explained at
     * http://creativecommons.org/licenses/publicdomain
     *
     * As of 2010/06/11, this method is identical to the (package private) hash method in OpenJDK 7's
     * java.util.HashMap class.
     */
    private static int smear(int hashCode) {
        hashCode ^= (hashCode >>> 20) ^ (hashCode >>> 12);
        return hashCode ^ (hashCode >>> 7) ^ (hashCode >>> 4);
    }
}
