package net.cnri.microservices;

import java.util.UUID;

public class IdGenerator {

    public static String generate() {
        String uuid = UUID.randomUUID().toString();
        return uuid.substring(uuid.length()-12);
    }
}
