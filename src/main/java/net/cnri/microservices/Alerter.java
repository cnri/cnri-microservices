package net.cnri.microservices;

public interface Alerter {
    void alert(String s);
}
