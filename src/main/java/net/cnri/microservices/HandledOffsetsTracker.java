package net.cnri.microservices;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HandledOffsetsTracker {
    private static final Logger logger = LoggerFactory.getLogger(HandledOffsetsTracker.class);
        
    private final ConcurrentMap<Long, Boolean> handled = new ConcurrentHashMap<>();
    private final String name;
    private final Alerter alerter;

    private long handledUpTo;

    public HandledOffsetsTracker(long initHandledUpTo, String name, Alerter alerter) {
        this.handledUpTo = initHandledUpTo;
        this.name = name;
        this.alerter = alerter;
    }
    
    public synchronized long handle(long offset) {
        if (offset <= handledUpTo) {
            logger.error("Committed handled offset " + offset + " in " + name + "; handledUpTo = " + handledUpTo);
            alerter.alert("Committed handled offset " + offset + " in " + name + "; handledUpTo = " + handledUpTo);
        } else if (handledUpTo + 1 == offset) {
            handledUpTo++; 
            while (handled.remove(handledUpTo + 1) != null) {
                handledUpTo++;
            }
        } else {
            handled.put(offset, Boolean.TRUE);
        }
        return handledUpTo;
    }
    
    public synchronized long skipTo(long offset) {
        while (handledUpTo < offset) {
            if (handled.remove(handledUpTo + 1) == null) {
                logger.error("Skipping over offset " + (handledUpTo + 1) + " in " + name + "!");
                alerter.alert("Skipping over offset " + (handledUpTo + 1) + " in " + name + "!");
            } 
            handledUpTo++;
        }
        while (handled.remove(handledUpTo + 1) != null) {
            handledUpTo++;
        }
        return handledUpTo;
    }
}
