package net.cnri.microservices;

public interface StripedExecutorService {
    void submit(Object stripe, Runnable task);
    void shutdown();
}
