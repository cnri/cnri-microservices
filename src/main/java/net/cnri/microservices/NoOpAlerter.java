package net.cnri.microservices;

public class NoOpAlerter implements Alerter {

    @Override
    public void alert(String s) {
        //no-op
    }

}
