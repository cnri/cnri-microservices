package net.cnri.microservices;

public class Message {

    private final String topic;
    private final String key;
    private final String message;

    public Message(String topic, String key, String message) {
        this.topic = topic;
        this.key = key;
        this.message = message;
    }

    public String getTopic() {
        return topic;
    }

    public String getKey() {
        return key;
    }

    public String getMessage() {
        return message;
    }
}
