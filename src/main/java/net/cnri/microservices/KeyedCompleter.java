package net.cnri.microservices;

import java.util.concurrent.CompletableFuture;

public interface KeyedCompleter<ResultT> {
    void init();
    void shutdown();
    CompletableFuture<ResultT> register(String key);
    void clear(String key);
}