package net.cnri.microservices;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KafkaMessageProducer implements MessageProducer {

    private static Logger logger = LoggerFactory.getLogger(KafkaMessageProducer.class);

    private final KafkaProducer<String, String> producer;

    public KafkaMessageProducer(String bootstrapServers) {
        this(bootstrapServers, null);
    }

    public KafkaMessageProducer(String bootstrapServers, Map<String, String> producerConfig) {
        Properties props = new Properties();
        if (producerConfig != null) props.putAll(producerConfig);
        props.putIfAbsent("bootstrap.servers", bootstrapServers);
        props.putIfAbsent("key.serializer", StringSerializer.class.getName());
        props.putIfAbsent("value.serializer", StringSerializer.class.getName());
        props.putIfAbsent("acks", "all");
        props.putIfAbsent("client.id", "producer-" + UUID.randomUUID());
        producer = new KafkaProducer<>(props);
    }

    @Override
    public void produce(Collection<Message> messages) {
        List<Future<RecordMetadata>> futures = new ArrayList<>();
        for (Message message : messages) {
            String topic = message.getTopic();
            String key = message.getKey();
            String messageString = message.getMessage();
        ProducerRecord<String, String> record = null;
        if (key != null) {
                record = new ProducerRecord<>(topic, key, messageString);
        } else {
                record = new ProducerRecord<>(topic, messageString);
        }
        Future<RecordMetadata> future = producer.send(record);
            futures.add(future);
        }
        for (Future<RecordMetadata> future : futures) {
        try {
            future.get();
                //            RecordMetadata result = future.get();
                //            logger.info("produce result: " + GsonUtility.getGson().toJson(result));
        } catch (InterruptedException | ExecutionException e) {
            logger.error("error getting future", e);
        }
        }
    }

    @Override
    public void shutdown() {
        producer.close();
    }

}
