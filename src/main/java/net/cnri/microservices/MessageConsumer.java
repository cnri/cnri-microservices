package net.cnri.microservices;

import java.util.function.Consumer;


public interface MessageConsumer {

    void start(String topic, Consumer<String> callback);
    void shutdown();
    String getTopic();
}
